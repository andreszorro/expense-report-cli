# -*- coding: utf-8 -*-

""" Add command """

from os import linesep
from datetime import date
import click
from app import util, file_setup
from app.util import validate_date, validate_choice

@click.command("add")
@click.pass_context
@click.option(
    "-d",
    "--date",
    "date",
    help="Pass a date to skip prompt",
    callback=lambda ctx, param, value: validate_date(value)
)
@click.option(
    "-p",
    "--place",
    "place",
    help="Pass a place to skip prompt"
)
@click.option(
    "-t",
    "--description",
    "description",
    help="Pass a description to skip prompt"
)
@click.option(
    "-v",
    "--value",
    "value",
    type=float,
    help="Pass a value to skip prompt"
)
@click.option(
    "-t",
    "--type",
    "arg_type",
    type=click.Choice([opt["return"] for opt in util.TYPE_OPTS]),
    help="Pass a type to skip prompt"
)
def execute(ctx, **args):
    data = ctx.obj["DATA"]

    click.secho("📝  Add an entry", fg="blue", bold=True)

    entry = {}
    entry["date"] = (
        args["date"] if args["date"]
        else click.prompt(
            click.style("Date", dim=True),
            default=date.today().isoformat(),
            value_proc=validate_date
        )
    )

    entry["place"] = (
        args["place"] if args["place"]
        else click.prompt(
            click.style("Place", dim=True)
        )
    )

    entry["value"] = (
        args["value"] if args["value"]
        else click.prompt(
            click.style("Value", dim=True),
            type=float,
            value_proc=float
        )
    )

    entry["type"] = (
        args["arg_type"] if args["arg_type"]
        else click.prompt(
            click.style("Type", dim=True),
            type=click.Choice([
                "{0}: {1}".format(idx + 1, opt["prompt"])
                for idx, opt in enumerate(util.TYPE_OPTS)
            ]),
            value_proc=lambda value: validate_choice(value, util.TYPE_OPTS),
            show_default=False,
            default=util.TYPE_OPTS[0]["return"]
        )
    )

    entry["description"] = (
        args["description"] if args["description"]
        else click.prompt(
            click.style("Description", dim=True)
        )
    )

    data["entries"].append(entry)

    file_setup.write_file(data)
    click.secho("{0}🌟  Entry added!{0}".format(linesep), fg="yellow")
    ctx.exit()
