# -*- coding: utf-8 -*-

""" Update command """

from os import linesep
import click
from app import util, file_setup
from app.util import validate_date, validate_choice

@click.command("update")
@click.pass_context
@click.option(
    "-d",
    "--date",
    "date",
    help="Pass a date to skip prompt",
    callback=lambda ctx, param, value: validate_date(value)
)
@click.option(
    "-p",
    "--place",
    "place",
    help="Pass a place to skip prompt"
)
@click.option(
    "-t",
    "--description",
    "description",
    help="Pass a description to skip prompt"
)
@click.option(
    "-v",
    "--value",
    "value",
    type=float,
    help="Pass a value to skip prompt"
)
@click.option(
    "-t",
    "--type",
    "arg_type",
    type=click.Choice([opt["return"] for opt in util.TYPE_OPTS]),
    help="Pass a type to skip prompt"
)
def execute(ctx, **args):
    data = ctx.obj["DATA"]

    should_prompt_other_values = len(list(filter(None, args.values()))) > 0

    click.secho("📝  Update an entry", fg="blue", bold=True)

    entries = [
        {
            "prompt": "{3}{4}. {0}: {1} (${2})".format(
                entry["date"],
                util.encode(entry["place"]),
                entry["value"],
                linesep,
                idx + 1
            ),
            "return": entry
        }
        for idx, entry in enumerate(sorted(data["entries"], key=lambda k: k["date"]))
    ]

    entry = click.prompt(
        click.style("Pick an entry to update:", dim=True),
        type=click.Choice([ent["prompt"] for ent in entries]),
        value_proc=lambda value: validate_choice(value, entries, show_choices=False),
        show_default=False
    )

    entry["date"] = args["date"] if args["date"] else (
        entry["date"] if should_prompt_other_values
        else click.prompt(
            click.style("Date:", dim=True),
            default=entry["date"],
            value_proc=validate_date
        )
    )

    entry["place"] = args["place"] if args["place"] else (
        entry["place"] if should_prompt_other_values
        else click.prompt(
            click.style("Place:", dim=True),
            default=entry["place"],
        )
    )

    entry["value"] = args["value"] if args["value"] else (
        float(entry["value"]) if should_prompt_other_values
        else click.prompt(
            click.style("Value:", dim=True),
            default=entry["value"],
            type=float,
            value_proc=float
        )
    )

    entry["type"] = args["arg_type"] if args["arg_type"] else (
        entry["type"] if should_prompt_other_values
        else click.prompt(
            click.style("Type:", dim=True),
            type=click.Choice([
                "{0}: {1}".format(idx + 1, opt["prompt"])
                for idx, opt in enumerate(util.TYPE_OPTS)
            ]),
            value_proc=lambda value: validate_choice(value, util.TYPE_OPTS),
            show_default=False,
            default=entry["type"]
        )
    )

    entry["description"] = args["description"] if args["description"] else (
        entry["description"] if should_prompt_other_values
        else click.prompt(
            click.style("Description:", dim=True),
            default=entry["description"]
        )
    )

    file_setup.write_file(data)
    click.secho("{0}🌟  Entry updated!{0}".format(linesep), fg="yellow")
    ctx.exit()
