# -*- coding: utf-8 -*-

""" Output command """

import re
from itertools import groupby
from os import linesep
from datetime import date
from string import capwords
import click
import pyperclip
from dateutil.relativedelta import relativedelta, FR
from app import util

NEXT_FRIDAY = relativedelta(
    date.today() + relativedelta(days=1, weekday=FR),
    date.today()
).days

def _flag(per_diem, total_minus_per_diem):
    return (
        "🤑" if total_minus_per_diem > round(per_diem * 0.3, 2)
        else "💵" if total_minus_per_diem > round(per_diem * 0.14, 2)
        else "😱"
    )

def _color(per_diem, total_minus_per_diem):
    return (
        "green" if total_minus_per_diem > round(per_diem * 0.3, 2)
        else "yellow" if total_minus_per_diem > round(per_diem * 0.14, 2)
        else "red"
    )

class Formatter():
    def __init__(self, data):
        self.data = data

    def _sum(self):
        return round(
            sum(
                [entry["value"] for entry in self.data["entries"]]
            ),
            2
        )
    def _per_day(self):
        grouped = [
            {
                "date": k,
                "total": round(sum(
                    [val["value"] for val in v]
                ), 2)
            }
            for k, v in groupby(
                self.data["entries"],
                key=lambda i: i["date"]
            )
        ]

        return sorted(grouped, key=lambda k: k["date"])

    # Formatters
    def total(self):
        total = self._sum()
        per_diem = self.data["per_diem"]
        total_minus_per_diem = round(float(per_diem) - total, 2)
        color = _color(per_diem, total_minus_per_diem)

        click.echo("You have spent " + click.style("${:.2f}".format(total), fg="yellow") + ".")
        click.echo(
            "You have " +
            click.style("${:.2f}".format(total_minus_per_diem), fg=color) +
            " left to spend."
        )
        click.echo("Next payday is in {0} days".format(click.style(str(NEXT_FRIDAY), fg="yellow")))

    def summary(self, daily=True):
        total = self._sum()
        per_diem = self.data["per_diem"]
        total_minus_per_diem = round(float(per_diem) - total, 2)

        flag = _flag(per_diem, total_minus_per_diem)
        color = _color(per_diem, total_minus_per_diem)

        click.echo("🔍  Summary report:" + linesep)

        click.echo("💰  Per Diem: " + click.style("${:.2f}".format(per_diem), fg="green"))
        click.echo("💸  Total spent: " + click.style("${:.2f}".format(total), fg="green"))
        click.echo(
            "🏝   Days to payday: " +
            click.style(str(NEXT_FRIDAY), fg="green")
        )
        click.echo(
            linesep + flag + "  Per Diem left: " +
            click.style("${:.2f}".format(total_minus_per_diem), fg=color)
        )


        per_day = self._per_day()

        if not per_day:
            return

        click.echo("{0}🗓  Daily report:{0}".format(linesep))

        if daily:
            for day in per_day:
                click.echo(
                    click.style(day["date"] + ": ", bold=True, fg="blue") +
                    "${:.2f}".format(day["total"])
                )


    def full(self):
        output = []
        text_to_copy = []
        for entry in sorted(self.data["entries"], key=lambda k: k["date"]):
            text_to_copy.append([
                util.format_date(entry["date"]),
                capwords(entry["place"]),
                [
                    ent["prompt"] for ent in util.TYPE_OPTS
                    if ent["return"] == entry["type"]
                ][0],
                "Yes",
                str(entry["value"]),
                "USD",
                "Cash / Other",
                self.data["name"],
                self.data["project"],
                entry["description"].capitalize()
            ])

            output.append([
                click.style(entry["date"], fg="blue", bold=True),
                click.style(entry["description"].capitalize(), fg="cyan"),
                capwords(entry["place"]),
                click.style("${:.2f}".format(entry["value"]), fg="green"),
            ])

        text_to_copy = linesep.join([
            ",".join(entry) for entry in text_to_copy
        ])

        self.summary(daily=False)

        word_len = []

        for entry in output:
            for word in entry:
                word_len.append(len(word))

        col_width = max(word_len) + 2 if word_len else 0

        for entry in output:
            click.echo(util.encode("  ".join(word.ljust(col_width) for word in entry)))

        if not text_to_copy:
            return click.secho(linesep + "😕  No entries to copy.", fg="yellow")

        pyperclip.copy(text_to_copy)
        click.secho(linesep + "📋  Output copied to clipboard!", fg="yellow")

@click.command("output")
@click.pass_context
@click.argument(
    "format_method",
    type=click.Choice([
        key for key in dir(Formatter) if not re.match(r"^_", key)
    ]),
    default="total"
)
def execute(ctx, format_method="total"):
    formatter = Formatter(ctx.obj["DATA"])

    getattr(formatter, format_method)()
    click.echo(linesep)
    ctx.exit()
