# -*- coding: utf-8 -*-

import sys
from os import linesep
from datetime import date, datetime
from click import BadParameter
from dateutil.parser import isoparse

# pylint: disable=too-few-public-methods,line-too-long

PY3 = sys.version_info[0] >= 3

TYPE_OPTS = [
    {"prompt": "Meals", "return": "meals"},
    {"prompt": "City / Transport", "return": "transport"},
]

def format_date(string, fmt="%y%b%-d"):
    return isoparse(string).strftime(fmt).lower()

# NOTE: when concatenating str and other types of data (unicode/byte)
# in Python 2 str is decoded automatically to unicode in order to
# finish the operation. You'll need to manually encode back to utf-8
# if you still need the string
# In Python 3 ALL text is Unicode, so no need to transform strings
# when concatenating
# See this for reference: https://stackoverflow.com/questions/31771758/concatenating-unicode-with-string-print-%C2%A3-1-works-but-print-%C2%A3-u1-t
# and this: https://docs.python.org/release/3.0.1/whatsnew/3.0.html#text-vs-data-instead-of-unicode-vs-8-bit
def encode(value):
    if PY3:
        value = value.decode("utf-8") if isinstance(value, bytes) else value
    else:
        value = value if isinstance(value, bytes) else value.encode("utf-8")
    return value

def validate_date(value):
    if value is None:
        return value

    try:
        date_value = isoparse(value)
        return date.fromtimestamp(date_value.timestamp()).isoformat()
    except AttributeError:
        return date.fromtimestamp(
            (date_value - datetime.fromtimestamp(0)).total_seconds()
        ).isoformat()
    except (TypeError, ValueError):
        raise BadParameter("{0} is not in ISO format (YYYY-MM-DD)".format(value))

def validate_choice(value, choices, show_choices=True):
    try:
        value = int(value) - 1
        if value < 0 or value >= len(choices):
            raise ValueError()

        return choices[value]["return"]
    except (TypeError, ValueError):
        raise BadParameter(
            "{0} is not a valid choice{1}{2}"
            .format(
                value + 1 if isinstance(value, int) else value,
                linesep,
                "valid choices:" + linesep + linesep.join([
                    "  {0}: {1}".format(idx + 1, encode(opt["prompt"]))
                    for idx, opt in enumerate(choices)
                ]) if show_choices else ""
            )
        )
