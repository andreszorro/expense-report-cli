# -*- coding: utf-8 -*-

from os import linesep
from re import match
import click
from app import commands, file_setup

__version__ = "0.1.2"
CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])

@click.group(
    context_settings=CONTEXT_SETTINGS,
    invoke_without_command=True
)
@click.version_option(__version__)
@click.pass_context
def main(ctx):
    click.secho(
        "{1}📗  Expense Report - Version: {0}{1}".format(__version__, linesep),
        fg="green"
    )

    file_check = file_setup.check_file()
    data = None

    if file_check is None:
        click.echo("It seems you haven't created a report yet")
        name = click.prompt(click.style("What is your name?", dim=True))
        project = click.prompt(click.style("What project are you assigned to?", dim=True))
        per_diem = click.prompt(click.style("Assigned weekly per diem?", dim=True), default="350")

        click.echo(
            "Creating report in {0}{1}"
            .format(
                click.style(file_setup.get_file_location(), fg="cyan"),
                linesep
            )
        )

        data = file_setup.create_file({
            "name": name,
            "project": project,
            "per_diem": float(per_diem)
        })
    elif file_check is False:
        click.echo(
            "It seems that the file located on {0} is outdated{1}"
            .format(
                click.style(file_setup.get_file_location(), fg="cyan"),
                linesep
            )
        )

        data = file_setup.read_file()
        if ctx.invoked_subcommand == "output" or ctx.invoked_subcommand is None:
            pass # do nothing
        elif click.confirm(
                click.style("Would you like to overwrite existing file?", dim=True),
                default=False
            ):
            data = file_setup.read_file()
            per_diem = click.prompt(
                click.style("Update per diem?", dim=True),
                default=str(int(data["per_diem"]))
            )
            data["per_diem"] = float(per_diem)
            data["entries"] = []
            click.echo(
                "Overwriting report in {0}{1}"
                .format(
                    click.style(file_setup.get_file_location(), fg="cyan"),
                    linesep
                )
            )
            data = file_setup.create_file(data)
    else:
        data = file_setup.read_file()

    ctx.ensure_object(dict)
    ctx.obj["DATA"] = data

    if ctx.invoked_subcommand is None:
        ctx.invoke(commands.output.execute)

for cmd in [key for key in dir(commands) if not match(r"^_", key)]:
    main.add_command(getattr(commands, cmd).execute)
