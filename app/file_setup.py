# -*- coding: utf-8 -*-

import json
try:
    from pathlib import Path
except ImportError:
    from pathlib2 import Path
from datetime import date
from dateutil.relativedelta import relativedelta, SA

def get_file_location():
    return str(Path.home()) + "/expense_report.json"

def check_file():
    data_file = Path(get_file_location())
    if not (data_file.exists() and data_file.read_text("utf-8")):
        return None

    next_pay = date.today() + relativedelta(days=1, weekday=SA)

    should_update = relativedelta(
        next_pay,
        date.fromtimestamp(data_file.stat().st_mtime)
    ).weeks

    if should_update > 0:
        return False

    return True

def read_file():
    data_file = Path(get_file_location())
    content = data_file.read_bytes()
    return json.loads(content, encoding="utf-8")

def write_file(data):
    data_file = Path(get_file_location())
    json_data = json.dumps(data, ensure_ascii=False)

    try:
        data_file.write_text(json_data, "utf-8")
    except TypeError:
        if isinstance(json_data, str) and hasattr(json_data, "decode"):
            json_data = json_data.decode("utf-8")
            data_file.write_text(json_data, "utf-8")
        else:
            raise
    return data

def create_file(data=None):
    if data is None:
        data = {}
    data_file = Path(get_file_location())
    data_file.touch()
    json_data = {
        "last_update": date.today().isoformat(),
        "name": data["name"] or "No Name",
        "project": data["project"] or "No Project",
        "per_diem": data["per_diem"] or 0.0,
        "entries": []
    }
    return write_file(json_data)
