Expense Report CLI
==================

Do your reporting through CLI an get totals/output report

Install
-------

To install, make sure you have [Python and pip installed](https://www.makeuseof.com/tag/install-pip-for-python/)

```bash
pip install git+https://bitbucket.org/andreszorro/expense-report-cli.git
```

if you want to upgrade:

```bash
pip install git+https://bitbucket.org/andreszorro/expense-report-cli.git --upgrade
```

Run:

```bash
exrep --help
```

TODO
----

- Detect name formatting (as previous script)
- OCR Scan Images
