# -*- coding: utf-8 -*-
import re
from setuptools import setup

VERSION = re.search(
    r"^__version__\s*=\s*\"(.*)\"",
    open("app/expense_report.py").read(),
    re.M
    ).group(1)

with open("README.md", "rb") as f:
    LONG_DESC = f.read().decode("utf-8")

setup(
    name="expense-report",
    version=VERSION,
    packages=["app", "app.commands"],
    long_description=LONG_DESC,
    entry_points={
        "console_scripts": ["exrep = app.expense_report:main"]
    },
    description="Write down expense reports",
    author="Andres Zorro",
    author_email="zorrodg@gmail.com",
    install_requires=[
        "python-dateutil",
        "click",
        "pyperclip"
    ],
    extras_require={
        ":python_version == '2.7'": [
            "backports-datetime-fromisoformat",
            "pathlib2"
        ],
        "dev": [
            "pylint",
            "pylint-quotes"
        ]
    }
)
